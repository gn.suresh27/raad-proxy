# raad-proxy

Nginx proxy to handle Django recipe API application

## Getting started

By default, all logs are visible to the public. So make GitLab’s pipeline outputs private. This will avoid credentials (ex: AWS secret key) being displayed to the public if an error occurs. 

#### Visibility setup
Changes to be made in two places:
- settings -> General -> visibility, project features & permissions
- CI/CD  -> Change to "Only project members"
- save changes 
- settings -> CI/CD -> General Pipelines
- Uncheck "Public Pipelines"
- save changes

#### Protected Branches
- settings -> repository -> Locate “Protected branches” and expand
> We are using the pipeline called “GitLab Flow”
> We restrict the deployment to only “Maintainers”
> We use a wild card branch (“*-release”) for restricting the deployment 
- Click on Branch -> *-release 
- Click “create wildcard *-release”
- Allowed to Merge -> “Maintainers”
- Allowed to push -> “Maintainers”
- Click "Protect"

``` bash 
Now any branch that ends with “-release” will be protected 
```

#### Protected Tags
- In the same screen above Click on Tags -> *-release 
- Click “create wildcard *-release”
- Allowed to Create -> “Maintainers”
- Click "Protect"
``` 
Now any Tags that ends with “-release” will be protected 
```
- Clone the project to Local machine
- Open `README.md` file and modify
- Commit changes
``` 
$ git add .
$ git commit -am “Added ReadMe to project”
$ git push origin
```
- Got to gitlab

### Create Repository in AWS - ECR

- AWS console -> ECR -> <Get started>
- Give the same name (**raad-proxy**) as the gitlab project for easy identification
- Set visibility to `Private`
- Leave the `“Tags immutable”` as disabled. This is because we will push images with same tags called `“-latest”` multiple times
- Enable `“Scan on push”` which will scan images for vulnerabilities while pushing to the repository
- Create Registry
- Check if the newly created registry is listed

- Create NGINX config files, tpl file, entrypoint.sh, uwsgi_params

Run Docker
- Enter docker commands and save the file.
Build the docker image with the tag “proxy”
- `$ docker build -t proxy .`
- `$ docker images`

Commit the work
- `$ git add .`
- `$ git commit -am “Added nginx proxy to project”`
- `$ git push origin`

- This will create a build job in the pipeline 
- Verify the success of the build job

- Create a merge request from `feature/nginx-proxy` to `main`
- When the merge request is finished, it will trigger a job to push `proxy:dev`

### Create Release branch (1.0-release) 
- `Repository -> branches -> <new branch>  “1.0-release”`
- This will trigger a pipeline job of Build (`if: "$CI_COMMIT_BRANCH == 'main'"`)

### Create Tags (1.0.0-release)
- `Repository -> tags -> <New Tag> “1.0.0-release”` from minor version `1.0-release`
- This will trigger a job for push (`if: "$CI_COMMIT_TAG =~ /^*-release$/"`)







